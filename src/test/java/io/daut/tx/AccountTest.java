package io.daut.tx;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class AccountTest {
    @Test
    public void testCredit() {
        final Account account = Account.createAccount();
        account.credit(BigDecimal.valueOf(10));
        final BigDecimal balance = account.getBalance();
        assertEquals(BigDecimal.valueOf(10), balance);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreditWithNegativeFundsShouldThrow() {
        final Account account = Account.createAccount();
        account.credit(BigDecimal.valueOf(-1));
    }

    @Test
    public void testDebit() {
        final Account account = Account.createAccount();
        account.credit(BigDecimal.valueOf(10));
        account.debit(BigDecimal.valueOf(5));
        final BigDecimal balance = account.getBalance();
        assertEquals(BigDecimal.valueOf(5), balance);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDebitWithNegativeFundsShouldThrow() {
        final Account account = Account.createAccount();
        account.credit(BigDecimal.valueOf(10));
        account.debit(BigDecimal.valueOf(-2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDebitWithFundsGreaterThanBalance() {
        final Account account = Account.createAccount();
        account.credit(BigDecimal.valueOf(10));
        account.debit(BigDecimal.valueOf(20));
    }

    @Test
    public void testGetBalance() {
        final Account account = Account.createAccount();
        final BigDecimal balance = account.getBalance();
        assertEquals(BigDecimal.valueOf(0), balance);
    }
}
