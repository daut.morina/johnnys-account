package io.daut.tx;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class TransactionServiceTest {
    @Test
    public void testProcessTransaction() {
        final Account account = Account.createAccount();
        final TransactionService transactionService = TransactionService.create(account);
        final Transaction transaction = Transaction.create(UUID.randomUUID(), BigDecimal.valueOf(100), "Granny's birthday gift");

        transactionService.processTransaction(transaction);

        final BigDecimal balance = account.getBalance();

        assertEquals(BigDecimal.valueOf(100), balance);
    }

    @Test
    public void testProcessTransactionWithDebit() {
        final Account account = Account.createAccount();
        account.credit(BigDecimal.valueOf(100));
        final TransactionService transactionService = TransactionService.create(account);
        final Transaction transaction = Transaction.create(UUID.randomUUID(), BigDecimal.valueOf(50), Transaction.Type.DEBIT, "Birthday Boy's Night Out");

        transactionService.processTransaction(transaction);

        final BigDecimal balance = account.getBalance();

        assertEquals(BigDecimal.valueOf(50), account.getBalance());
    }

    @Test
    public void testProcessTransactionWithDebitWithInsufficientFunds() {
        final Account account = Account.createAccount();
        account.credit(BigDecimal.valueOf(100));
        final TransactionService transactionService = TransactionService.create(account);
        final Transaction transaction = Transaction.create(UUID.randomUUID(), BigDecimal.valueOf(200), Transaction.Type.DEBIT, "Birthday Boy's Night Out");

        transactionService.processTransaction(transaction);

        assertEquals(BigDecimal.valueOf(100), account.getBalance());
        assertEquals(Transaction.State.FAILED, transaction.getState());
    }

    @Test
    public void testProcessTransactionWithCreditWithAlreadyProcessedTransaction() {
        final Account account = Account.createAccount();
        account.credit(BigDecimal.valueOf(100));
        final TransactionService transactionService = TransactionService.create(account);
        final Transaction transaction = Transaction.create(UUID.randomUUID(), BigDecimal.valueOf(100), Transaction.Type.CREDIT, "Birthday Boy's Night Out");
        transaction.successful();

        transactionService.processTransaction(transaction);

        assertEquals(BigDecimal.valueOf(100), account.getBalance());
    }

    @Test
    public void test() throws Exception {
        final Account account = Account.createAccount();
        final TransactionService transactionService = TransactionService.create(account);

        final List<Transaction> transactions = List.of(
                Transaction.create(BigDecimal.valueOf(100), Transaction.Type.CREDIT),
                Transaction.create(BigDecimal.valueOf(50), Transaction.Type.CREDIT),
                Transaction.create(BigDecimal.valueOf(20), Transaction.Type.CREDIT)
        );

        final ArrayBlockingQueue<Transaction> transactionsQueue = new ArrayBlockingQueue<>(transactions.size(), true, transactions);

        ExecutorService es = Executors.newCachedThreadPool();

        for (int i = 0; i < transactionsQueue.size(); i++) {
            es.execute(() -> {
                System.out.println(transactionsQueue.size());
                final Transaction transaction = transactionsQueue.poll();
                if (transaction != null) {
                    System.out.println(transaction.getAmount());
                    transactionService.processTransaction(transaction);
                } else {
                    System.out.println("wooops");
                }
            });
        }

        es.shutdown();

        final boolean finished = es.awaitTermination(10, TimeUnit.SECONDS);
        if (!finished) throw new Exception("Boom!");

        assertEquals(BigDecimal.valueOf(170), account.getBalance());
    }
}
