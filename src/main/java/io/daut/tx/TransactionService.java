package io.daut.tx;

import java.math.BigDecimal;

public class TransactionService {
    private final Account account;

    public static TransactionService create(Account account) {
        return new TransactionService(account);
    }

    private TransactionService(Account account) {
        this.account = account;
    }

    public void processTransaction(Transaction transaction) {
        if (transaction.getState() != Transaction.State.PENDING) return;

        switch (transaction.getType()) {
            case CREDIT:
                credit(transaction);
                break;
            case DEBIT:
                debit(transaction);
                break;
        }
    }

    private void credit(Transaction transaction) {
        final BigDecimal amount = transaction.getAmount();

        try {
            this.account.credit(amount);
        } catch (IllegalArgumentException e) {
            transaction.failed();
        }
    }

    private void debit(Transaction transaction) {
        final BigDecimal amount = transaction.getAmount();

        try {
            this.account.debit(amount);
        } catch (IllegalArgumentException e) {
            transaction.failed();
        }
    }
}
