package io.daut.tx;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

public class Transaction {
    public enum Type {
        CREDIT,
        DEBIT
    }

    public enum State {
        PENDING,
        SUCCESSFUL,
        FAILED,
    }

    private final UUID id;
    private final BigDecimal amount;
    private final Type type;
    private State state;
    private final String description;

    public static Transaction create(BigDecimal amount, Type type) {
        return new Transaction(UUID.randomUUID(), amount, type, State.PENDING, null);
    }

    public static Transaction create(UUID id, BigDecimal amount, String description) {
        return new Transaction(id, amount, Type.CREDIT, State.PENDING, description);
    }

    public static Transaction create(UUID id, BigDecimal amount, Type type, String description) {
        return new Transaction(id, amount, type, State.PENDING, description);
    }

    private Transaction(UUID id, BigDecimal amount, Type type, State state, String description) {
        assert id != null;
        this.id = id;

        assert amount != null;
        this.amount = amount;

        assert type != null;
        this.type = type;

        assert state != null;
        this.state = state;

        this.description = description;
    }

    public void successful() {
        this.state = State.SUCCESSFUL;
    }

    public void failed() {
        this.state = State.FAILED;
    }

    public UUID getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Type getType() {
        return type;
    }

    public State getState() {
        return state;
    }

    public Optional<String> getDescription() {
        return Optional.ofNullable(description);
    }
}
