package io.daut.tx;

import java.math.BigDecimal;

public class Account {
    private BigDecimal balance;

    public static Account createAccount() {
        return new Account();
    }

    private Account() {
        this.balance = BigDecimal.valueOf(0);
    }

    public void credit(BigDecimal funds) {
        synchronized (this) {
            if (funds.compareTo(BigDecimal.ZERO) < 0) throw new IllegalArgumentException("Funds must not be negative");
            this.balance = this.balance.add(funds);
        }
    }

    public void debit(BigDecimal funds) {
        synchronized (this) {
            if (funds.compareTo(BigDecimal.ZERO) < 0) throw new IllegalArgumentException("Funds must not be negative");
            if (funds.compareTo(this.balance) > 0) throw new IllegalArgumentException("Funds must not be greater than Balance");
            this.balance = this.balance.subtract(funds);
        }
    }

    public BigDecimal getBalance() {
        return this.balance;
    }
}
